package me.randomhashtags.randompackage.utils.classes.envoy;

import me.randomhashtags.randompackage.RandomPackageAPI;
import me.randomhashtags.randompackage.utils.universal.UMaterial;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Firework;
import org.bukkit.inventory.ItemStack;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

public class EnvoyCrate {
	public static HashMap<String, EnvoyCrate> crates;
	public static String defaultTier;
	private static RandomPackageAPI api;

	private YamlConfiguration yml;
	private String ymlName;
	private int chance;

	private UMaterial block, fallingblock;
	private String rewardSize;
	private List<String> rewards;
	private List<UMaterial> cannotLandAbove;
	private boolean dropsFromSky, canRepeatRewards;
	private ItemStack item;
	private Firework fw;

	public EnvoyCrate(File f) {
		if(crates == null) {
			crates = new HashMap<>();
			api = RandomPackageAPI.getAPI();
		}
		yml = YamlConfiguration.loadConfiguration(f);
		ymlName = f.getName().split("\\.yml")[0];
		chance = yml.getInt("chance");
		dropsFromSky = yml.getBoolean("settings.drops from sky");
		canRepeatRewards = yml.getBoolean("settings.can repeat rewards");

		crates.put(ymlName, this);
	}

	public YamlConfiguration getYaml() { return yml; }
	public String getYamlName() { return ymlName; }

	public Firework getFirework() {
		if(fw == null) {
			final String[] f = yml.getString("firework").split(":");
			fw = api.createFirework(FireworkEffect.Type.valueOf(f[0].toUpperCase()), api.getColor(f[1]), api.getColor(f[2]), Integer.parseInt(f[3]));
		}
		return fw;
	}
	public int getChance() { return chance; }
	public UMaterial getBlock() {
		if(block == null) block = UMaterial.match(yml.getString("settings.block"));
		return block;
	}
	public boolean dropsFromSky() { return dropsFromSky; }
	public UMaterial getFallingBlock() {
		if(fallingblock == null) fallingblock = UMaterial.match(yml.getString("settings.falling block"));
		return fallingblock;
	}
	public boolean canRepeatRewards() { return canRepeatRewards; }
	public String getRewardSize() {
		if(rewardSize == null) rewardSize = yml.getString("settings.reward size");
		return rewardSize;
	}
	public List<UMaterial> cannotLandAbove() {
		if(cannotLandAbove == null) {
			cannotLandAbove = new ArrayList<>();
			for(String s : yml.getStringList("settings.cannot land above")) {
				cannotLandAbove.add(UMaterial.match(s));
			}
		}
		return cannotLandAbove;
	}
	public ItemStack getItem() {
		if(item == null) item = api.d(yml, "item");
		return item.clone();
	}
	public List<String> getRewards() {
		if(rewards == null) rewards = yml.getStringList("rewards");
		return rewards;
	}

	public int getRandomRewardSize() {
		final String[] s = getRewardSize().split("-");
		final boolean c = rewardSize.contains("-");
		final int min = c ? Integer.parseInt(s[0]) : Integer.parseInt(rewardSize), max = c ? Integer.parseInt(s[1]) : -1;
		return min+(max == -1 ? 0 : new Random().nextInt(max-min+1));
	}
	public List<String> getRandomRewards() {
		final List<String> rewards = new ArrayList<>(this.getRewards()), actualrewards = new ArrayList<>();
		final Random random = new Random();
		for(int i = 1; i <= getRandomRewardSize(); i++) {
			if(rewards.size() != 0) {
				final String reward = rewards.get(random.nextInt(rewards.size()));
				final String[] a = reward.split(";chance=");
				if(random.nextInt(100) <= api.getRemainingInt(a[1])) {
					actualrewards.add(a[0]);
					if(!canRepeatRewards) rewards.remove(reward);
				} else {
					i -= 1;
				}
			}
		}
		return actualrewards;
	}
	public List<ItemStack> getRandomizedRewards() {
		final List<String> r = getRandomRewards();
		final List<ItemStack> a = new ArrayList<>();
		for(String s : r) {
			final ItemStack i = api.d(null, s);
			if(i != null && !i.getType().equals(Material.AIR)) a.add(i);
		}
		return a;
	}
	public boolean canLand(Location spawnLocation) {
		final World w = spawnLocation.getWorld();
		final Block b = w.getBlockAt(new Location(w, spawnLocation.getBlockX(), spawnLocation.getBlockY()-1, spawnLocation.getBlockZ()));
		if(cannotLandAbove().contains(UMaterial.match(b.getType().name()))) return false;
		return true;
	}
	public static EnvoyCrate valueOf(ItemStack is) {
		if(crates != null && is != null && is.hasItemMeta())
			for(EnvoyCrate c : crates.values())
				if(is.isSimilar(c.getItem()))
					return c;
		return null;
	}
	public static EnvoyCrate getRandomCrate(boolean useChances) {
		if(crates != null) {
			final Random random = new Random();
			if(useChances) {
				for(EnvoyCrate c : crates.values())
					if(random.nextInt(100) <= c.getChance())
						return c;
			} else {
				return crates.get(defaultTier);
			}
			return crates.get(defaultTier);
		}
		return null;
	}
	public static void deleteAll() {
		crates = null;
		defaultTier = null;
		api = null;
	}
}
