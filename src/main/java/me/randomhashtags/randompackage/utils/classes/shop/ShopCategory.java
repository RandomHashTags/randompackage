package me.randomhashtags.randompackage.utils.classes.shop;

import me.randomhashtags.randompackage.api.Shop;
import me.randomhashtags.randompackage.utils.universal.UInventory;
import me.randomhashtags.randompackage.utils.universal.UMaterial;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ShopCategory {
    public static HashMap<String, ShopCategory> categories;
    private static Shop shop;

    private YamlConfiguration yml;
    private String ymlName, inventoryTitle;
    private UInventory inventory;
    private List<ShopItem> items;

    public ShopCategory(File f) {
        if(categories == null) {
            categories = new HashMap<>();
            shop = Shop.getShop();
        }
        yml = YamlConfiguration.loadConfiguration(f);
        ymlName = f.getName().split("\\.yml")[0];
        categories.put(ymlName, this);
    }
    public YamlConfiguration getYaml() { return yml; }
    public String getYamlName() { return ymlName; }
    public String getInventoryTitle() {
        if(inventoryTitle == null) inventoryTitle = ChatColor.translateAlternateColorCodes('&', yml.getString("title"));
        return inventoryTitle;
    }
    public UInventory getInventory() {
        if(inventory == null) {
            inventory = new UInventory(null, yml.getInt("size"), getInventoryTitle());
            final Inventory ii = inventory.getInventory();
            items = new ArrayList<>();
            final ItemStack back = shop.back;
            for(String s : yml.getConfigurationSection("gui").getKeys(false)) {
                final String p = yml.getString("gui." + s + ".prices");
                final String[] o = p != null ? p.split(";") : null;
                final String custom = yml.getString("gui." + s + ".custom.item"), d = yml.getString("gui." + s + ".item").toUpperCase();
                final boolean isBack = d.equals("BACK");
                final UMaterial u = isBack ? UMaterial.match(back) : UMaterial.match(d);
                final ItemStack display = isBack ? back : shop.d(yml, "gui." + s);
                final ItemStack purchased = !isBack ? custom != null ? shop.d(yml, "gui." + s + ".custom") : shop.d(null, d) : null;
                final int slot = yml.getInt("gui." + s + ".slot");
                items.add(new ShopItem(s, slot, yml.getString("gui." + s + ".opens"), display, purchased == null && u != null ? u.getItemStack() : purchased, o != null ? Double.parseDouble(o[0]) : 0, o != null ? Double.parseDouble(o[1]) : 0));
                ii.setItem(slot, display);
            }
        }
        return inventory;
    }
    public List<ShopItem> getShopItems() {
        if(items == null) getInventory();
        return items;
    }
    public ShopItem getItem(int slot) {
        for(ShopItem i : getShopItems()) {
            if(i.slot == slot) {
                return i;
            }
        }
        return null;
    }
    public static void deleteAll() {
        categories = null;
        shop = null;
    }
}
