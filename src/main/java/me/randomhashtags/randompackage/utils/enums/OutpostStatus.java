package me.randomhashtags.randompackage.utils.enums;

public enum OutpostStatus {UNCONTESTED, CONTESTED, CONTROLLING, UNDER_ATTACK, SECURING}
